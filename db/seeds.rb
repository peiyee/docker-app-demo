# frozen_string_literal: true

10.times do
  Post.create(
    name: Faker::Name.unique.name,
    title: Faker::Lorem.sentence,
    content: Faker::Lorem.paragraph
  )
end

